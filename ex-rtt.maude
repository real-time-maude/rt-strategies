***(

ex-rtt.maude 

A simple periodic RTT protocol, like 'ping'

Every 5 seconds, a sender node sends a message to the receiver to compute the
round trip time between the two nodes.

The messages are delayed and can take any value in some time interval,
otherwise computing the RTT would make no sense.

This simple example has both clocks, timers, and nondeterministic message
delays, so it covers a lot often needed in distributed protocols.

***)


load ./rtm-strategies .


omod RTT is
  including TIMED-PRELUDE .
  protecting NAT-TIME-DOMAIN-WITH-INF .  --- unclear whether INF needed
  pr RTM-COMMANDS{ Qid, Nat} .

  --- ---------------------------------
  var M            : Msg .
  vars TI TI'      : TimeInf .
  vars T T' T2 T3  : Time .
  vars R S         : Oid .
  vars C1 C2 STATE : Configuration .
  vars CONF        : Configuration .
  var ATTS         : AttributeSet .
  var N            : Nat .
  --- ---------------------------------

  sort DlyMsg .
  subsorts Msg < DlyMsg < Configuration < System .

  op dly : Msg Time Time -> DlyMsg [ctor] .  --- upper and lower bounds

  rl [deliver] : dly(M, 0, TI) => M .    --- can deliver ripe message any time
  
  msgs rttReq_from_to_ rttResp_from_to_ : Time Oid Oid -> Msg .

  class Sender | clock : Time, timer : Time,
                 lowerDly : Time, upperDly : TimeInf, 
                 rtt : TimeInf, receiver : Oid, period : Time .
		 
  class Receiver | lowerDly : Time, upperDly : TimeInf .  

  rl [send] :
     < S : Sender | clock : T, timer : 0, period : T2,
                    lowerDly : T3, upperDly : TI, receiver : R >
    =>
     < S : Sender | timer : T2 >
     dly(rttReq T from S to R, T3, TI) .

  rl [respond] :
     (rttReq T from S to R)
     < R : Receiver | lowerDly : T3, upperDly : TI >
    =>
     < R : Receiver | >
     dly(rttResp T from R to S, T3, TI) .

  rl [recordRTT] :
     (rttResp T from R to S)
     < S : Sender | clock : T2 >
    =>
     < S : Sender | rtt : T2 monus T > .

  --- entirely standard EXECUTABLE OO tick rule:
  crl [tick] :
      {STATE} => {timeEffect(STATE, T)} in time T
        if T <= mte(STATE) [nonexec] .

  op mte : Configuration -> TimeInf [frozen] .
  eq mte(none) = 0 .
  ceq mte(C1 C2) = min(mte(C1), mte(C2)) if C1 =/= none and C2 =/= none .

  eq mte(< S : Sender | timer : TI >) = TI .
  eq mte(< R : Receiver | >) = INF .
  eq mte(dly(M, T, TI)) = TI .
  eq mte(M) = 0 .     --- ripe message must be read immediately

  op rtt? : Configuration -> Bool .
  eq rtt?(none) = false .
  ceq rtt?(C1 C2) = rtt?(C1) or-else rtt?(C2) if C1 =/= none and C2 =/= none .
  eq rtt?(< S : Sender | rtt : TI >) = TI =/= INF and-then TI =/= zero .
  eq rtt?(< R : Receiver | >) = false .
  eq rtt?(dly(M, T, TI)) = false .
  eq rtt?(M) = false .

  op timeEffect : Configuration Time -> Configuration .
  eq timeEffect(none, T) = none .
  ceq timeEffect(C1 C2, T) = timeEffect(C1, T) timeEffect(C2, T) 
          if C1 =/= none and C2 =/= none .
  eq timeEffect(< S : Sender | clock : T, timer : TI >, T2)
   = < S : Sender | clock : T + T2, timer : TI monus T2 > .
  eq  timeEffect(< R : Receiver | >, T) = < R : Receiver | > .
  eq timeEffect(dly(M, T, TI), T2) = dly(M, T monus T2, TI monus T2) .
  eq timeEffect(M, T) = M .

  ops snd rcv : -> Oid [ctor] .

  op init : -> StrState .
  eq init
   = ({< snd : Sender | clock : 0, timer : 0, period : 5000,
                       lowerDly : 5, upperDly : 20, rtt : INF, receiver : rcv >
      < rcv : Receiver | lowerDly : 7, upperDly : 30 >} in time 0 ) | ('C |-> 0) .

  --- Skipping a round of the protocol (see strategies below)
  rl [skipRound] :
     < S : Sender | timer : 0, period : T2 >
    =>
     < S : Sender | timer : T2, period : T2 > [nonexec] .
endom

eof 
--- Timed simulation 
red tsim [1] in 'RTT : init 
    using delay or action with sampling max-time with default 4 until 10000 .

--- Finding states where RTT = 50 (or 20) with different sampling strategies 
red tsearch [2] in 'RTT : init => matches ( { CONF < S : Sender | rtt : 50, ATTS > } in time T ) 
    using delay or action with sampling max-time with default 4 .
red tsearch [2] in 'RTT : init => matches ( { CONF < S : Sender | rtt : 20, ATTS:AttributeSet > } in time R:Time ) 
    using delay or action with sampling fixed-time 1 .

--- Bounded analysis 
red tsearch [2] in 'RTT : init => matches ( { CONF < S : Sender | rtt : 50, ATTS > } in time T ) 
    using delay or action with sampling max-time with default 4 in time [5000, 10000] .
--- No RTT=20 with the max-time strategy 
red tsearch [2] in 'RTT : init => matches ( { CONF < S : Sender | rtt : 20, ATTS > } in time T ) 
    using delay or action with sampling max-time with default 4 in time [5000, 10000] .

--- Utimed search 
red usearch [2] in 'RTT : init => matches ( { CONF < S : Sender | rtt : 50, ATTS > } in time T ) 
    using delay or action with sampling max-time with default 4 .

--- Finding earliest and latest values
red find earliest  in 'RTT : init => matches ({STATE} in time T2) s.t. (rtt?(STATE)) using action or delay with sampling fixed-time 1 .
red find latest  in 'RTT : init => matches ({STATE} in time T2) s.t. (rtt?(STATE)) using action or delay with sampling fixed-time 1 .

--- Mixed strategies
red tsearch [2] in 'RTT : init => matches ( { CONF < S : Sender | rtt : 20, ATTS > } in time T ) 
    using delay or action with sampling
         switch (when (matches ({ STATE dly(M, T, T') } in time R:Time) ) do  fixed-time 1)
         otherwise  max-time with default 4  .

--- Testing the new strategy that reduces the state space

red size(tsearch   in  'RTT : init => matches ({STATE} in time T2) 
         using delay or  action 
    with sampling max-time with default 4 in time [0, 100000]) .

--- With this strategy, less states are visited (some rounds are skipped)
red size(tsearch   in  'RTT : init => matches ({STATE} in time T2) 
         using delay or  (
         if (matches (({STATE < S : Sender | timer : 0, ATTS:AttributeSet >} in time T)) )
            then (if (matches ('C |-> N) s.t. N <= 1)
                 then (apply 'send  ; (get ('C |-> N) and set ('C |-> N + 1)))
                 else (apply 'skipRound ; (get ('C |-> N) and set ('C |-> 0))) )
            else action)
    with sampling max-time with default 4 in time [0, 100000]) .
