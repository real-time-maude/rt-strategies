# Timed  Strategies for Real-Time Rewrite Theories

We propose a language for conveniently defining  execution strategies for
real-time rewrite theories, and provide Maude-strategy-implemented versions of
most [Real-Time Maude](https://olveczky.se/RealTimeMaude/index.html) analysis
methods, albeit with user-defined discrete and timed strategies.  
The details of the new strategy language, its semantics and the available 
commands can be found in this [technical report](https://arxiv.org/abs/2403.08920). 


## Getting started

The project was tested in [Maude 3.2.1](http://maude.cs.illinois.edu/).

### Maude files

Next we describe the main Maude files of the implementation. 
See the headers of each file for further information.

- `rtm-prelude.maude`: Sorts defining the time domain (discrete and continuous
  case) and the state of the system with a global clock .
- `rtm-strategies.maude`: Definition of user-defined discrete and sampling
  strategies. This module uses `metaSrewrite`,to implement several analysis
  commands for real-time rewrite theories. 
- `ex-0.maude`: A simple rewrite-theory with several examples of analyses and
  strategies.
- `ex-rtt.maude`: A  periodic RTT protocol, like 'ping'
- `ex-cash.maude`: New version of the CASH case study. The strategy-based
  commands in the end of the file are used to find the missed deadline. 
- `ex-case-1-tick.maude`: CASH system with one-tick. This file is only needed
  to compare the performance of the strategy language and Maude's `search`
  command. 
- `ex-case-untimed.maude`: CASH system with one-tick and removing the global
  clock. This file is only needed to compare the performance of the strategy
  language and Maude's `search` command. 
